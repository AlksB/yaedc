cmake_minimum_required(VERSION 3.15.0)

#set(CMAKE_VERBOSE_MAKEFILE ON)

# Без этого в винде фейлится проверка работоспособности компилятора.
SET (CMAKE_C_COMPILER_WORKS 1)
SET (CMAKE_CXX_COMPILER_WORKS 1)

set(CMAKE_SYSTEM_NAME Generic)

set(CMAKE_CXX_STANDARD 14)

set(CMAKE_C_COMPILER arm-none-eabi-gcc)
set(CMAKE_CXX_COMPILER arm-none-eabi-g++)
set(CMAKE_ASM_COMPILER arm-none-eabi-gcc)
set(CMAKE_OBJCOPY arm-none-eabi-objcopy)
set(CMAKE_OBJDUMP arm-none-eabi-objdump)

project(yaedc C CXX ASM)

file(GLOB_RECURSE USER_SOURCES
        "${PROJECT_SOURCE_DIR}/Src/*.c"
        "${PROJECT_SOURCE_DIR}/Src/*.cpp"
        "${PROJECT_SOURCE_DIR}/usb-midi/src/*.cpp"
        )

SET(STARTUP_SOURCE
        "${PROJECT_SOURCE_DIR}/startup_stm32f407xx.s"
        )

SET(HAL_SOURCES
        "${PROJECT_SOURCE_DIR}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_rcc.c"
        "${PROJECT_SOURCE_DIR}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_utils.c"
        "${PROJECT_SOURCE_DIR}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_gpio.c"
        "${PROJECT_SOURCE_DIR}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_adc.c"
        "${PROJECT_SOURCE_DIR}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_dma.c"
        "${PROJECT_SOURCE_DIR}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_tim.c"
        )

SET(FREERTOS_SOURCES
        "${PROJECT_SOURCE_DIR}/freertos/Source/croutine.c"
        "${PROJECT_SOURCE_DIR}/freertos/Source/event_groups.c"
        "${PROJECT_SOURCE_DIR}/freertos/Source/list.c"
        "${PROJECT_SOURCE_DIR}/freertos/Source/port.c"
        "${PROJECT_SOURCE_DIR}/freertos/Source/queue.c"
        "${PROJECT_SOURCE_DIR}/freertos/Source/stream_buffer.c"
        "${PROJECT_SOURCE_DIR}/freertos/Source/tasks.c"
        "${PROJECT_SOURCE_DIR}/freertos/Source/timers.c"
        "${PROJECT_SOURCE_DIR}/freertos/Source/MemMang/heap_1.c"
        "${PROJECT_SOURCE_DIR}/freertos/Source/freertos_default_hooks.c")

SET(LIBUSB_STM32_SOURCES
        "${PROJECT_SOURCE_DIR}/libusb_stm32/src/usbd_core.c"
        "${PROJECT_SOURCE_DIR}/libusb_stm32/src/usbd_stm32f429_otgfs.c"
        "${PROJECT_SOURCE_DIR}/libusb_stm32/src/usbd_core.c")

add_library(DSP_LIBRARY
        "${PROJECT_SOURCE_DIR}/dsp/sig/src/sig.cpp")

include_directories(Inc)
include_directories(Drivers/STM32F4xx_HAL_Driver/Inc)
include_directories(Drivers/CMSIS/Include)
include_directories(Drivers/CMSIS/Device/ST/STM32F4xx/Include)
include_directories(Drivers/CMSIS/Device/ST)
include_directories(dsp/sig/inc)
include_directories(freertos/Source/include)
include_directories(freertos/)
include_directories(libusb_stm32/inc)
include_directories(usb-midi/inc)

set(LINKER_SCRIPT "${PROJECT_SOURCE_DIR}/STM32F407VGTx_FLASH.ld")

# core flags
set(CORE_FLAGS "-mthumb -mcpu=cortex-m4 -mlittle-endian -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb-interwork --specs=nano.specs")

# compiler: language specific flags
set(CMAKE_C_FLAGS "${CORE_FLAGS} -fno-builtin -Wall -std=gnu99 -fdata-sections -ffunction-sections -O0 -g")

set(CMAKE_CXX_FLAGS "${CORE_FLAGS} -fno-rtti -fno-exceptions -fno-builtin -Wall -std=gnu++11 -fdata-sections -ffunction-sections -O0 -g")

set(CMAKE_ASM_FLAGS "${CORE_FLAGS} -g -D__USES_CXX")

set(CMAKE_EXE_LINKER_FLAGS "-Wl,-gc-sections -Wl,-Map=${PROJECT_NAME}.map -T ${LINKER_SCRIPT}")

add_definitions(-DSTM32F4 -DSTM32F407xx -DUSE_FULL_LL_DRIVER -DHSE_VALUE=8000000)

add_executable(${PROJECT_NAME}.elf ${USER_SOURCES} ${STARTUP_SOURCE} ${HAL_SOURCES} ${FREERTOS_SOURCES} ${LIBUSB_STM32_SOURCES})

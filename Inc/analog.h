#pragma once
#include "adc.h"
#include "gpio.h"
#include "tim_base.h"

class Analog {
public:
    Analog(){};
    static Analog& instance(){
        static Analog obj{};
        return obj;
    };

    static int init();
    static void adc_dma_isr() {instance().adc1.dma_isr();};
    static uint16_t get(size_t ch_num);
    
private:
    static void analog_thread (void * p_context);
    static constexpr size_t channels_num = 8;
    void adc_tim_init();
    static void pins_init();

    tim_base tim{TIM2};
    const adc_channel analog_pin_array[Analog::channels_num] = {
        {GPIOA, LL_GPIO_PIN_1, LL_ADC_CHANNEL_1, LL_ADC_REG_RANK_1},
        {GPIOA, LL_GPIO_PIN_2, LL_ADC_CHANNEL_2, LL_ADC_REG_RANK_2},
        {GPIOA, LL_GPIO_PIN_3, LL_ADC_CHANNEL_3, LL_ADC_REG_RANK_3},
        {GPIOB, LL_GPIO_PIN_0, LL_ADC_CHANNEL_8, LL_ADC_REG_RANK_4},
        {GPIOB, LL_GPIO_PIN_1, LL_ADC_CHANNEL_9, LL_ADC_REG_RANK_5},
        {GPIOC, LL_GPIO_PIN_1, LL_ADC_CHANNEL_11, LL_ADC_REG_RANK_6},
        {GPIOC, LL_GPIO_PIN_2, LL_ADC_CHANNEL_12, LL_ADC_REG_RANK_7},
        {GPIOC, LL_GPIO_PIN_5, LL_ADC_CHANNEL_15, LL_ADC_REG_RANK_8}
    };

    const adc_cfg adc1_cfg = {
        .adc = ADC1,
        .channels = analog_pin_array,
        .ch_num = Analog::channels_num,
        .trigger_source = LL_ADC_REG_TRIG_EXT_TIM2_TRGO,
        .dma_channel = LL_DMA_CHANNEL_0,
        .dma_stream = LL_DMA_STREAM_0,
    };

    adc adc1 {&adc1_cfg};

    uint16_t buff[channels_num]{0};
};
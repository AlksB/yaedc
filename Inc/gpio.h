#pragma once
#include "stm32f4xx_ll_gpio.h"

#define OTG_FS_DM_PIN LL_GPIO_PIN_11
#define OTG_FS_DP_PIN LL_GPIO_PIN_12
#define OT_FS_ID_PIN LL_GPIO_PIN_10
#define OTG_FS_GPIO GPIOA

#define OTG_FS_POWER_ON_GPIO GPIOC
#define OTG_FS_POWER_ON_PIN LL_GPIO_PIN_0

#define LED3_PIN LL_GPIO_PIN_13
#define LED3_GPIO GPIOD

#define LED4_PIN LL_GPIO_PIN_12
#define LED4_GPIO GPIOD

#define LED5_PIN LL_GPIO_PIN_14
#define LED5_GPIO GPIOD

#define LED6_PIN LL_GPIO_PIN_15
#define LED6_GPIO GPIOD

#define BUTTON_PIN LL_GPIO_PIN_0
#define BUTTON_GPIO GPIOA

#define USART3_TX_PIN LL_GPIO_PIN_8
#define USART3_RX_PIN LL_GPIO_PIN_9
#define USART3_GPIO GPIOD

void gpio_init(void);
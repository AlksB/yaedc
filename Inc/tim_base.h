#pragma once
#include "stm32f4xx_ll_tim.h"

class tim_base {
public:
    tim_base(TIM_TypeDef * tim) : tim(tim){
    }

public:
    void isr(void);

    void set_trigger_output (uint32_t TimerSynchronization); // TIM_LL_EC_TRGO
    int set_update_freq (uint32_t freq);
    void clk_enable();
    void start(bool update_interrupt_enable);
    uint32_t get_tim_bus_freq();

private:
    TIM_TypeDef *tim = nullptr;
};

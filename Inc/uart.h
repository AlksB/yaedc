#pragma once

#include "stm32f407xx.h"
#include "FreeRTOS.h"
#include "semphr.h"

class uart {
  public:
    uart (){};
    static int init(uint32_t baudrate);
    static int write(const uint8_t *buff, uint16_t len);
    static void dma_isr();

  private:
    static uart &instance() {
      static uart obj{};
      return obj;
    }

   static int set_baudrate(uint32_t baudrate);
    static int clock_init();
    static int dma_init();
    static void interrupt_init();

  private:
    USART_TypeDef *const hw{USART3};
    DMA_TypeDef *const dma{DMA1};
    DMA_Stream_TypeDef *const dma_tx_stream{DMA1_Stream4};
    const uint8_t tx_stream_channel{7};
    SemaphoreHandle_t sem = nullptr;
};

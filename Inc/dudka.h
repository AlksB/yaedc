#ifndef _DUDKA_H_
#define _DUDKA_H_

#include "FreeRTOS.h"
#include "analog.h"
#include "task.h"
#include "usb-midi.h"
#include <array>
#include <cstdint>
#include <limits>
#include <stdint.h>

class Dudka {
  public:
    Dudka(Analog &analog, uint8_t p_ch_num, uint8_t n_ch_num)
        : _analog(analog), _p_ch_num(p_ch_num), _n_ch_num(n_ch_num){};
    int calibrate_offset();
    int16_t get_val();
    static std::array<MIDI_EventPacket_t, 2> get_breath_controller(int16_t val);

  private:
    Analog &_analog;
    const uint8_t _p_ch_num;
    const uint8_t _n_ch_num;
    int _delta_max = 25000;
    int _threshold = 200;
    uint16_t _p_offset = 0;
    uint16_t _n_offset = 0;

    static constexpr int16_t max_value{std::numeric_limits<int16_t>::max()};
    static constexpr int calibration_time_ms = 1000;
    static constexpr int calibration_sampling_time_ms = 1;

    static void thread(void *p_context);
};

#endif // _DUDKA_H_
#pragma once
#include "stm32f4xx_ll_adc.h"
#include "stm32f4xx_ll_dma.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include <cerrno>

struct adc_channel {
    GPIO_TypeDef *gpio;
    uint32_t pin;
    uint32_t adc_ch;
    uint32_t rank;
};

struct adc_cfg {
    ADC_TypeDef *const adc;
    const adc_channel *channels;
    const uint32_t ch_num;
    const uint32_t trigger_source;
    const uint32_t dma_channel;
    const uint32_t dma_stream;
};

class adc {
public:
    adc (const adc_cfg *cfg) : cfg(cfg) {
    }

public:
    int init ();

    void start ();
        
    int attach_channels ();

    void adc_isr ();

    void dma_isr ();

    int adc_get (uint8_t ch_num, uint16_t *buff, size_t buff_size);

    int adc_sync();

public:
    static const uint8_t CH_NUM = 8;
    static const uint8_t SAMPLES_NUM = 16;

private:
    int dma_init ();
    void clk_enable ();
    int init_channels ();
    void irq_enable ();

private:
    uint16_t buff[adc::CH_NUM * adc::SAMPLES_NUM * 2];
    uint16_t *buff_ptr;

private:
    SemaphoreHandle_t sem = nullptr;
    static const uint32_t SEM_WAIT_TIME_MS = 10;

private:
    const adc_cfg *cfg = nullptr;
};

#include "tim_base.h"
#include "stm32f4xx_ll_bus.h"
#include "stm32f4xx_ll_rcc.h"
#include "errno.h"

void tim_base::isr() {
    if (LL_TIM_IsActiveFlag_UPDATE(this->tim)){
        LL_TIM_ClearFlag_UPDATE(this->tim);
    }
}

void tim_base::start(bool update_interrupt_enable) {
    if (update_interrupt_enable){
        LL_TIM_EnableIT_UPDATE(this->tim);
    }

    LL_TIM_EnableCounter(this->tim);
}

int tim_base::set_update_freq (uint32_t freq) {
    LL_TIM_InitTypeDef tim_init = {0};
    LL_TIM_StructInit(&tim_init);

    uint32_t clk = this->get_tim_bus_freq();
    uint32_t ratio = (clk/freq);
    uint16_t presc = ratio/0x10000;
    auto per = static_cast<uint16_t>((clk/static_cast<uint32_t>(presc+1))/freq);
    tim_init.Prescaler = presc;
    tim_init.Autoreload = per;
    tim_init.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;
    if ((LL_TIM_Init(this->tim, &tim_init)) != SUCCESS){
        return EIO;
    }

    return 0;
}

void tim_base::set_trigger_output(uint32_t TimerSynchronization) {
    LL_TIM_SetTriggerOutput(this->tim, TimerSynchronization);
}

void tim_base::clk_enable () {
    switch (reinterpret_cast<uint32_t>(this->tim)) {
#ifdef TIM1
        case TIM1_BASE:
            LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_TIM1);
            break;
#endif

#ifdef TIM2
        case TIM2_BASE:
            LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM2);
            break;
#endif

#ifdef TIM3
        case TIM3_BASE:
            LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM3);
            break;
#endif

#ifdef TIM4
        case TIM4_BASE:
            LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM4);
            break;
#endif

#ifdef TIM5
        case TIM5_BASE:
            LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM5);
            break;
#endif

#ifdef TIM6
        case TIM6_BASE:
            LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM6);
            break;
#endif

#ifdef TIM7
        case TIM7_BASE:
            LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM7);
            break;
#endif

#ifdef TIM8
        case TIM8_BASE:
            LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_TIM8);
            break;
#endif

#ifdef TIM9
        case TIM9_BASE:
            LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_TIM9);
            break;
#endif

#ifdef TIM10
        case TIM10_BASE:
            LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_TIM10);
            break;
#endif

#ifdef TIM11
        case TIM11_BASE:
            LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_TIM11);
            break;
#endif

#ifdef TIM12
        case TIM12_BASE:
            LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM12);
            break;
#endif

#ifdef TIM13
        case TIM13_BASE:
            LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM13);
            break;
#endif

#ifdef TIM14
        case TIM14_BASE:
            LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM14);
            break;
#endif
    }
}

uint32_t tim_base::get_tim_bus_freq () {
    LL_RCC_ClocksTypeDef rcc_clk = {0};
    LL_RCC_GetSystemClocksFreq(&rcc_clk);

    switch (reinterpret_cast<uint32_t>(this->tim)) {

#ifdef TIM2
        case TIM2_BASE:
#endif
#ifdef TIM3
        case TIM3_BASE:
#endif
#ifdef TIM4
        case TIM4_BASE:
#endif
#ifdef TIM5
        case TIM5_BASE:
#endif
#ifdef TIM6
        case TIM6_BASE:
#endif
#ifdef TIM7
        case TIM7_BASE:
#endif
#ifdef TIM12
        case TIM12_BASE:
#endif
#ifdef TIM13
        case TIM13_BASE:
#endif
#ifdef TIM14
        case TIM14_BASE:
#endif
            return rcc_clk.PCLK1_Frequency * ((LL_RCC_GetAPB1Prescaler() == LL_RCC_APB1_DIV_1)?1:2);

#ifdef TIM1
        case TIM1_BASE:
#endif
#ifdef TIM8
        case TIM8_BASE:
#endif
#ifdef TIM9
        case TIM9_BASE:
#endif
#ifdef TIM10
        case TIM10_BASE:
#endif
#ifdef TIM11
        case TIM11_BASE:
#endif
            return rcc_clk.PCLK2_Frequency * ((LL_RCC_GetAPB2Prescaler() == LL_RCC_APB2_DIV_1)?1:2);
    }

    return 0;
}
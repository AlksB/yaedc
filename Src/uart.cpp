#include "rcc.h"
#include "uart.h"

int uart::init(uint32_t baudrate) {
    static uart &obj = uart::instance();
    int rv = 0;
    if ((obj.sem = xSemaphoreCreateBinary()) == nullptr) {
        return -1;
    }

    rv = obj.clock_init();
    if (rv) {
        return rv;
    }

    rv = obj.set_baudrate(baudrate);
    if (rv) {
        return rv;
    }   

    rv = dma_init();
    if (rv) {
        return rv;
    }   

  
    obj.hw->CR3 |= USART_CR3_DMAT;
    obj.hw->CR1 |= USART_CR1_TE | USART_CR1_UE;
    
    if (xSemaphoreGive(obj.sem) != pdTRUE) {
        rv = -1;
    }
 
    return rv;
}

int uart::dma_init() {
    static uart &obj = uart::instance();
    if (obj.hw != USART3) {
        return -1;
    }

    if (obj.tx_stream_channel > 7) {
        return -1;
    }

    switch (reinterpret_cast<uint32_t>(obj.dma)) {
        case (reinterpret_cast<uint32_t>(DMA1_BASE)):
            RCC->AHB1ENR |= RCC_AHB1ENR_DMA1EN;
            break;
        case (reinterpret_cast<uint32_t>(DMA2_BASE)):
            RCC->AHB1ENR |= RCC_AHB1ENR_DMA2EN;
            break;
        default:
            return -1;
    }

    obj.dma_tx_stream->CR |= (obj.tx_stream_channel << DMA_SxCR_CHSEL_Pos) |
        DMA_SxCR_MINC | DMA_SxCR_DIR_0 |
        DMA_SxCR_TCIE | DMA_SxCR_TEIE;
    obj.dma_tx_stream->PAR = reinterpret_cast<uint32_t>(&obj.hw->DR);

    NVIC_SetPriority(DMA1_Stream4_IRQn, 5);
    NVIC_EnableIRQ(DMA1_Stream4_IRQn);

   
    return 0;
}

int uart::write(const uint8_t *buff, uint16_t len) {
    static uart &obj = uart::instance();
    int ret = 0;

    if ((len == 0) || (buff == nullptr)) {
        return -1;
    }

    if (xSemaphoreTake(obj.sem, portMAX_DELAY) != pdTRUE) {
        return -1;
    }

    obj.dma_tx_stream->M0AR = reinterpret_cast<uint32_t>(buff);
    obj.dma_tx_stream->NDTR = len;
    obj.dma_tx_stream->CR |= DMA_SxCR_EN;
 
    return ret;
}

void uart::dma_isr() {
    static uart &obj = uart::instance();
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    if (DMA1->HISR && DMA_HISR_TCIF4) {
        xSemaphoreGiveFromISR(obj.sem, &xHigherPriorityTaskWoken);
        DMA1->HIFCR = DMA_HIFCR_CTCIF4;
    }

    if (DMA1->HISR && DMA_HISR_TEIF4) {
        DMA1->HIFCR = DMA_HIFCR_CTEIF4;
    }

    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

int uart::set_baudrate(uint32_t baudrate) {
    static uart &obj = uart::instance();
    uint32_t pclk = 0;
    int ret = -1;
    switch (reinterpret_cast<uint32_t>(obj.hw)) {
    case (USART2_BASE):
    case (USART3_BASE):
    case (UART4_BASE):
    case (UART5_BASE):
        pclk = rcc_get_APB1PCLK();
        ret = 0;
        break;
    case (USART1_BASE):
    case (USART6_BASE):
        pclk = rcc_get_APB2PCLK();
        ret = 0;
        break;
    default:
        ret = -1;
        break;
    }

    if (!ret) {
        uint32_t div = pclk / baudrate;
        uint16_t mantissa = div / 8;
        uint8_t frac = div % 8;
        obj.hw->CR1 |= USART_CR1_OVER8;
        obj.hw->BRR = (mantissa << USART_BRR_DIV_Mantissa_Pos) |
                        (frac << USART_BRR_DIV_Fraction_Pos);
    }

    return ret;
}

int uart::clock_init() {
    static uart &obj = uart::instance();
    int rv = 0;
    switch (reinterpret_cast<uint32_t>(obj.hw)) {
    case (USART2_BASE):
        RCC->APB1ENR |= RCC_APB1ENR_USART2EN;
        break;
    case (USART3_BASE):
        RCC->APB1ENR |= RCC_APB1ENR_USART3EN;
        break;
    case (UART4_BASE):
        RCC->APB1ENR |= RCC_APB1ENR_UART4EN;
        break;
    case (UART5_BASE):
        RCC->APB1ENR |= RCC_APB1ENR_UART5EN;
        break;
    case (USART1_BASE):
        RCC->APB2ENR |= RCC_APB2ENR_USART1EN;
        break;
    case (USART6_BASE):
        RCC->APB2ENR |= RCC_APB2ENR_USART6EN;
        break;
    default:
        rv = -1;
        break;
    }

    return rv;
}

#include "FreeRTOS.h"
#include "analog.h"
#include "dudka.h"
#include "gpio.h"
#include "rcc.h"
#include "stm32f4xx_ll_tim.h"
#include "task.h"
#include "tim_base.h"
#include "uart.h"
#include "usb-midi.h"

tim_base rtos_tim(TIM7);
extern "C" void vPortSetupTimerInterrupt(void) {
    rtos_tim.clk_enable();
    rtos_tim.set_update_freq(1000);
    rtos_tim.start(true);
    NVIC_SetPriority(TIM7_IRQn, configLIBRARY_LOWEST_INTERRUPT_PRIORITY);
    NVIC_EnableIRQ(TIM7_IRQn);
}

extern "C" void cdc_init(void);

void midi_test_task(void *p) {
    Dudka dudka{Analog::instance(), 0, 1};
    while (1) {
        vTaskDelay(1);
        if (LL_GPIO_IsInputPinSet(BUTTON_GPIO, BUTTON_PIN)) {
            dudka.calibrate_offset();
        }

        auto val = dudka.get_val();
        auto message = Dudka::get_breath_controller(val);
        usb_midi::instance().send(message.data(), 1);
    }
}

void uart_test_task(void *p) {
    while (1) {
        const uint8_t message[] = "test\n\r";
        uart::write(message, sizeof(message));
        vTaskDelay(pdMS_TO_TICKS(100));
    }
}

int midi_test_init(void) {
    TaskHandle_t t_handle;

    if (xTaskCreate(midi_test_task, "MIDI", 300, nullptr, configMAX_PRIORITIES,
                    &t_handle) != pdTRUE) {
        return ENOMEM;
    }

    return 0;
}

int uart_test_init(void) {
    TaskHandle_t t_handle;

    if (xTaskCreate(uart_test_task, "uart_tset", 300, nullptr, configMAX_PRIORITIES,
                    &t_handle) != pdTRUE) {
        return ENOMEM;
    }

    return 0;
}

int uart_init(void) { return uart::init(115200); }

int main(void) {
    rcc_config();

    gpio_init();

    Analog::init();

    usb_midi::instance().init();

    midi_test_init();

    uart_init();

    uart_test_init();

    vTaskStartScheduler();
}

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void) {
    while (1)
        ;
}

#ifdef USE_FULL_ASSERT

void assert_failed(uint8_t *file, uint32_t line) {
    while (1)
        ;
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

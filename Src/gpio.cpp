#include "gpio.h"
#include "analog.h"
#include "stm32f4xx_ll_bus.h"

void gpio_init(void) {
    LL_GPIO_InitTypeDef init_s = {0};

    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOA);
    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOB);
    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOC);
    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOD);

    LL_GPIO_StructInit(&init_s);
    init_s.Mode = LL_GPIO_MODE_OUTPUT;
    init_s.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
    init_s.Pin = LED3_PIN | LED4_PIN | LED5_PIN | LED5_PIN;
    init_s.Speed = LL_GPIO_SPEED_FREQ_MEDIUM;
    LL_GPIO_Init(LED3_GPIO, &init_s);

    LL_GPIO_StructInit(&init_s);
    init_s.Mode = LL_GPIO_MODE_ALTERNATE;
    init_s.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
    init_s.Pin = OTG_FS_DM_PIN | OTG_FS_DP_PIN | OT_FS_ID_PIN;
    init_s.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
    init_s.Alternate = LL_GPIO_AF_10;
    LL_GPIO_Init(OTG_FS_GPIO, &init_s);

    // USART3
    LL_GPIO_StructInit(&init_s);
    init_s.Mode = LL_GPIO_MODE_ALTERNATE;
    init_s.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
    init_s.Alternate = LL_GPIO_AF_7;
    init_s.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
    init_s.Pull = LL_GPIO_PULL_UP;
    init_s.Pin = USART3_TX_PIN | USART3_RX_PIN;
    LL_GPIO_Init(USART3_GPIO, &init_s);
}


#include "adc.h"
#include "stm32f4xx_ll_bus.h"

int adc::init () {
    this->clk_enable();

    if ((this->sem = xSemaphoreCreateBinary()) == nullptr) {
        return ENOMEM;
    }

    if (this->cfg == nullptr)
        return EINVAL;

    if (this->cfg->ch_num < 2)
        return EINVAL;

    LL_ADC_CommonInitTypeDef comm_init = {0};
    LL_ADC_CommonStructInit(&comm_init);
    comm_init.CommonClock = LL_ADC_CLOCK_SYNC_PCLK_DIV8;
    LL_ADC_CommonInit(ADC123_COMMON, &comm_init);

    LL_ADC_InitTypeDef adc_init = {0};
    LL_ADC_StructInit(&adc_init);
    if (LL_ADC_Init(this->cfg->adc, &adc_init) != SUCCESS) {
        return EIO;
    }

    int rv = 0;

    if ((rv = this->init_channels()) != 0) {
        return rv;
    }

    this->attach_channels();

    LL_ADC_REG_SetDMATransfer(this->cfg->adc, LL_ADC_REG_DMA_TRANSFER_UNLIMITED);

    if ((rv = this->dma_init()) != 0) {
        return rv;
    }

    return rv;
}

int adc::adc_get (uint8_t ch_num, uint16_t *buff, size_t buff_size) {
    if (buff_size < sizeof(uint16_t) * SAMPLES_NUM) {
        return EINVAL;
    }

    if (ch_num >= adc::CH_NUM) {
        return EINVAL;
    }

    uint16_t *b = this->buff_ptr + ch_num;
    for (uint32_t i = 0; i < adc::SAMPLES_NUM; i++) {
        buff[i] = *b;
        b += adc::CH_NUM;
    }

    return 0;
}

int adc::adc_sync () {
    if (xSemaphoreTake(this->sem, pdMS_TO_TICKS(adc::SEM_WAIT_TIME_MS)) != pdTRUE) {
        return ETIMEDOUT;
    }

    return 0;
}

void adc::start () {
    LL_DMA_EnableStream(DMA2, this->cfg->dma_stream);

    //LL_ADC_EnableIT_EOCS(this->cfg->adc);
    LL_ADC_Enable(this->cfg->adc);
    LL_ADC_REG_StartConversionExtTrig(this->cfg->adc, LL_ADC_REG_TRIG_EXT_RISING);
}

void adc::adc_isr () {
    if (LL_ADC_IsActiveFlag_EOCS(this->cfg->adc)) {
        LL_ADC_ClearFlag_EOCS(this->cfg->adc);
    }
}

void adc::dma_isr () {
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    if (LL_DMA_IsActiveFlag_TC0(DMA2)) {
        LL_DMA_ClearFlag_TC0(DMA2);
        this->buff_ptr = this->buff + ((adc::SAMPLES_NUM*adc::CH_NUM) / 2);
        xSemaphoreGiveFromISR(this->sem, &xHigherPriorityTaskWoken);
    } else if (LL_DMA_IsActiveFlag_HT0(DMA2)) {
        LL_DMA_ClearFlag_HT0(DMA2);
        this->buff_ptr = this->buff;
        xSemaphoreGiveFromISR(this->sem, &xHigherPriorityTaskWoken);
    }

    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

int adc::init_channels () {
    LL_ADC_REG_InitTypeDef reg_init = {0};
    LL_ADC_REG_StructInit(&reg_init);
    reg_init.TriggerSource = this->cfg->trigger_source;
    switch (this->cfg->ch_num) {
        case 2:
            reg_init.SequencerLength = LL_ADC_REG_SEQ_SCAN_ENABLE_2RANKS;
            reg_init.SequencerDiscont = LL_ADC_REG_SEQ_DISCONT_2RANKS;
            break;
        case 3:
            reg_init.SequencerLength = LL_ADC_REG_SEQ_SCAN_ENABLE_3RANKS;
            reg_init.SequencerDiscont = LL_ADC_REG_SEQ_DISCONT_3RANKS;
            break;
        case 4:
            reg_init.SequencerLength = LL_ADC_REG_SEQ_SCAN_ENABLE_4RANKS;
            reg_init.SequencerDiscont = LL_ADC_REG_SEQ_DISCONT_4RANKS;
            break;
        case 5:
            reg_init.SequencerLength = LL_ADC_REG_SEQ_SCAN_ENABLE_5RANKS;
            reg_init.SequencerDiscont = LL_ADC_REG_SEQ_DISCONT_5RANKS;
            break;
        case 6:
            reg_init.SequencerLength = LL_ADC_REG_SEQ_SCAN_ENABLE_6RANKS;
            reg_init.SequencerDiscont = LL_ADC_REG_SEQ_DISCONT_6RANKS;
            break;
        case 7:
            reg_init.SequencerLength = LL_ADC_REG_SEQ_SCAN_ENABLE_7RANKS;
            reg_init.SequencerDiscont = LL_ADC_REG_SEQ_DISCONT_7RANKS;
            break;
        case 8:
            reg_init.SequencerLength = LL_ADC_REG_SEQ_SCAN_ENABLE_8RANKS;
            reg_init.SequencerDiscont = LL_ADC_REG_SEQ_DISCONT_8RANKS;
            break;
        default:
            return EINVAL;

    }

    if (LL_ADC_REG_Init(this->cfg->adc, &reg_init) != SUCCESS) {
        return EIO;
    }

    return 0;
}

int adc::attach_channels () {
    for (size_t i = 0; i < this->cfg->ch_num; i++) {
        LL_ADC_REG_SetSequencerRanks(this->cfg->adc, this->cfg->channels[i].rank, this->cfg->channels[i].adc_ch);
        LL_ADC_SetChannelSamplingTime(this->cfg->adc, this->cfg->channels[i].adc_ch, LL_ADC_SAMPLINGTIME_15CYCLES);
    }

    return 0;
}

int adc::dma_init () {

    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_DMA2);

    LL_DMA_InitTypeDef init_struct = {0};
    LL_DMA_StructInit(&init_struct);
    init_struct.PeriphOrM2MSrcAddress = reinterpret_cast<uint32_t>(&this->cfg->adc->DR);
    init_struct.MemoryOrM2MDstAddress = reinterpret_cast<uint32_t>(this->buff);
    init_struct.Direction = LL_DMA_DIRECTION_PERIPH_TO_MEMORY;
    init_struct.Mode = LL_DMA_MODE_CIRCULAR;
    init_struct.PeriphOrM2MSrcIncMode = LL_DMA_PERIPH_NOINCREMENT;
    init_struct.MemoryOrM2MDstIncMode = LL_DMA_MEMORY_INCREMENT;
    init_struct.PeriphOrM2MSrcDataSize = LL_DMA_PDATAALIGN_HALFWORD;
    init_struct.MemoryOrM2MDstDataSize = LL_DMA_PDATAALIGN_HALFWORD;
    init_struct.NbData = sizeof(this->buff)/sizeof(this->buff[0]);
    init_struct.Channel = this->cfg->dma_channel;
    init_struct.Priority = LL_DMA_PRIORITY_LOW;
    init_struct.FIFOMode = LL_DMA_FIFOMODE_DISABLE;
    init_struct.FIFOThreshold = LL_DMA_FIFOTHRESHOLD_1_4;
    init_struct.MemBurst = LL_DMA_MBURST_SINGLE;
    init_struct.PeriphBurst = LL_DMA_PBURST_SINGLE;
    if (LL_DMA_Init(DMA2, this->cfg->dma_stream, &init_struct) != SUCCESS) {
        return EIO;
    }

    LL_DMA_EnableIT_TC(DMA2, this->cfg->dma_stream);
    LL_DMA_EnableIT_HT(DMA2, this->cfg->dma_stream);

    return 0;
}

void adc::clk_enable () {
    switch (reinterpret_cast<uint32_t>(this->cfg->adc)) {
#ifdef ADC1
        case ADC1_BASE:
            LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_ADC1);
            break;
#endif
#ifdef ADC2
        case ADC2_BASE:
            LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_ADC2);
            break;
#endif
#ifdef ADC3
        case ADC3_BASE:
            LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_ADC3);
            break;
#endif
    }
}

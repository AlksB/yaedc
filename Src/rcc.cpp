#include "rcc.h"
#include "stm32f4xx_ll_rcc.h"
#include "stm32f4xx_ll_utils.h"

void rcc_config() {
    ErrorStatus status = ERROR;
    LL_UTILS_PLLInitTypeDef pll_init{
        .PLLM = LL_RCC_PLLM_DIV_8, .PLLN = 336, .PLLP = LL_RCC_PLLP_DIV_2};
    LL_UTILS_ClkInitTypeDef clk_init{.AHBCLKDivider = LL_RCC_SYSCLK_DIV_1,
                                     .APB1CLKDivider = LL_RCC_APB1_DIV_4,
                                     .APB2CLKDivider = LL_RCC_APB2_DIV_2};

    LL_RCC_PLL_ConfigDomain_48M(LL_RCC_PLLSOURCE_HSE, pll_init.PLLM,
                                pll_init.PLLN, LL_RCC_PLLQ_DIV_7);

    status = LL_PLL_ConfigSystemClock_HSE(HSE_VALUE, LL_UTILS_HSEBYPASS_OFF,
                                          &pll_init, &clk_init);
    if (status != SUCCESS) {
        NVIC_SystemReset();
    }
}

uint32_t rcc_get_APB1PCLK() {
    LL_RCC_ClocksTypeDef clocks = {0};
    LL_RCC_GetSystemClocksFreq(&clocks);
    return clocks.PCLK1_Frequency;
}

uint32_t rcc_get_APB2PCLK() {
    LL_RCC_ClocksTypeDef clocks = {0};
    LL_RCC_GetSystemClocksFreq(&clocks);
    return clocks.PCLK2_Frequency;
}
#include "dudka.h"
#include "projdefs.h"
#include <cstdint>
#include <limits>
#include <stdint.h>

int Dudka::calibrate_offset() {
    const auto tick_start = xTaskGetTickCount();
    uint32_t avg_n = 0, avg_p = 0;
    size_t samples = 0;
    for (auto t = xTaskGetTickCount();
         (t - tick_start) < pdMS_TO_TICKS(calibration_time_ms);
         t = xTaskGetTickCount()) {
        avg_n += _analog.get(_n_ch_num);
        avg_p += _analog.get(_p_ch_num);
        samples++;
        vTaskDelay(pdMS_TO_TICKS(calibration_sampling_time_ms));
    }

    avg_n /= samples;
    avg_p /= samples;
    _p_offset = avg_p;
    _n_offset = avg_n;

    return 0;
}

int16_t Dudka::get_val() {
    int val = static_cast<int>(_analog.get(_p_ch_num)) -
              static_cast<int>(_analog.get(_n_ch_num));
    int val_offset = static_cast<int>(_p_offset) - static_cast<int>(_n_offset);
    val = val - val_offset;
    if (val > _delta_max) {
        val = _delta_max;
    }

    val = val * static_cast<int>(max_value) / _delta_max;
    if (val < _threshold) {
        val = 0;
    }

    return {static_cast<int16_t>(val)};
}

std::array<MIDI_EventPacket_t, 2> Dudka::get_breath_controller(int16_t val) {
    val = val < 0 ? 0 : val;
    uint8_t msb = (val >> (15 - 7)) & 0x7f;
    uint8_t lsb = (val >> (15 - 7 * 2)) & 0x7f;
    const MIDI_EventPacket_t pack_msb = {
        .Event = MIDI_EVENT(0x00, MIDI_COMMAND_CONTROL_CHANGE),
        .Data1 = 0xb0,
        .Data2 = 0x02,
        .Data3 = msb};
    const MIDI_EventPacket_t pack_lsb = {
        .Event = MIDI_EVENT(0x00, MIDI_COMMAND_CONTROL_CHANGE),
        .Data1 = 0xb0,
        .Data2 = 0x22,
        .Data3 = lsb};
    return {pack_msb, pack_lsb};
}
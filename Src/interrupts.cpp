#include "adc.h"
#include "analog.h"
#include "gpio.h"
#include "stm32f4xx_ll_gpio.h"
#include "tim_base.h"
#include "usb-midi.h"
#include "uart.h"

extern usb_midi usb_midi;
extern tim_base tim;
extern tim_base rtos_tim;

extern "C" {

void xPortSysTickHandler(void);
/******************************************************************************/
/*           Cortex-M4 Processor Interruption and Exception Handlers          */
/******************************************************************************/
/**
 * @brief This function handles Non maskable interrupt.
 */
void NMI_Handler(void) {}

/**
 * @brief This function handles Hard fault interrupt.
 */
void HardFault_Handler(void) {
    while (1) {
    }
}

/**
 * @brief This function handles Memory management fault.
 */
void MemManage_Handler(void) {
    while (1) {
    }
}

/**
 * @brief This function handles Pre-fetch fault, memory access fault.
 */
void BusFault_Handler(void) {
    while (1) {
    }
}

/**
 * @brief This function handles Undefined instruction or illegal state.
 */
void UsageFault_Handler(void) {
    while (1) {
    }
}

/**
 * @brief This function handles Debug monitor.
 */
void DebugMon_Handler(void) {}

/**
 * @brief This function handles System tick timer.
 */
void SysTick_Handler(void) {}

/******************************************************************************/
/* STM32F4xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32f4xx.s).                    */
/******************************************************************************/

void TIM2_IRQHandler(void) {}

void ADC_IRQHandler(void) {}

void DMA2_Stream0_IRQHandler(void) { Analog::adc_dma_isr(); }

void DMA1_Stream4_IRQHandler(void) { uart::dma_isr(); }

void TIM7_IRQHandler(void) {
    xPortSysTickHandler();
    rtos_tim.isr();
}

void OTG_FS_IRQHandler(void) { usb_midi.irq_handler(); }
}

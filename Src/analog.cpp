#include "analog.h"

void Analog::adc_tim_init (void){
    tim.clk_enable();
    tim.set_update_freq(8000*16);

    tim.set_trigger_output(LL_TIM_TRGO_UPDATE);

    tim.start(true);

    //NVIC_SetPriority(TIM2_IRQn, 6);
    //NVIC_EnableIRQ(TIM2_IRQn);
}

void Analog::analog_thread (void * p_context){
    uint16_t buff[adc::SAMPLES_NUM];
    while(true){
        for(size_t i = 0; i < adc::CH_NUM; i++) {
            instance().adc1.adc_sync();
            instance().adc1.adc_get(i, buff, sizeof(buff));
            uint16_t avg{0};
            for (auto &sample : buff) {
                avg += sample;
            }

            instance().buff[i] = avg;
        }
    }
}

void Analog::pins_init() {
    LL_GPIO_InitTypeDef init_s = {0};
    LL_GPIO_StructInit(&init_s);
    size_t i = 0;
    for (i = 0; i < (sizeof(instance().analog_pin_array) / sizeof(adc_channel)); i++) {
        init_s.Mode = LL_GPIO_MODE_ANALOG;
        init_s.Pin = instance().analog_pin_array[i].pin;
        LL_GPIO_Init(instance().analog_pin_array[i].gpio, &init_s);
    }
}

int Analog::init(){
    int rv = 0;

    pins_init();

    if ((rv = instance().adc1.init()) != 0){
        return rv;
    }

    instance().adc_tim_init();

    TaskHandle_t t_handle;

    if (xTaskCreate(analog_thread, "signal", 300, nullptr, configMAX_PRIORITIES, &t_handle) != pdTRUE){
        return ENOMEM;
    }

    NVIC_SetPriority(ADC_IRQn, 6);
    NVIC_EnableIRQ(ADC_IRQn);

    NVIC_SetPriority(DMA2_Stream0_IRQn, 6);
    NVIC_EnableIRQ(DMA2_Stream0_IRQn);

    instance().adc1.start();
    return 0;
}

uint16_t Analog::get(size_t ch_num) {
    if (ch_num >= channels_num) {
        return 0xffff;
    }

    return instance().buff[ch_num];
};
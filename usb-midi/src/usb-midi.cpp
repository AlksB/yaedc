#include "gpio.h"
#include "stm32f4xx.h"
#include "usb-midi.h"
#include <cstddef>

int usb_midi::init() {
    usbd_init(&instance().udev, &usbd_otgfs, instance().EP0_SIZE,
              instance().ubuf, sizeof(instance().ubuf));
    usbd_reg_config(&instance().udev, usb_midi::setconf);
    usbd_reg_control(&instance().udev, usb_midi::control);
    usbd_reg_descr(&instance().udev, usb_midi::getdesc);

    NVIC_SetPriority(OTG_FS_IRQn, 5);
    NVIC_EnableIRQ(OTG_FS_IRQn);

    usbd_enable(&instance().udev, true);
    usbd_connect(&instance().udev, true);

    return 0;
}

int usb_midi::send(MIDI_EventPacket_t &event) {
    if (instance().curr_conf_ != configuration::active) {
        return EFAULT;
    }

    int len = usbd_ep_write(&instance().udev, instance().EP_IN_ADDR, &event,
                            sizeof(event));
    if (len != sizeof(event)) {
        return EFAULT;
    }

    return 0;
}

int usb_midi::send(MIDI_EventPacket_t *events, size_t num) {
    if (instance().curr_conf_ != configuration::active) {
        return EFAULT;
    }
    int size = sizeof(events[0]) * num;
    int len =
        usbd_ep_write(&instance().udev, instance().EP_IN_ADDR, events, size);
    if (len != size) {
        return EFAULT;
    }

    return 0;
}

void usb_midi::ep_rx(usbd_device *dev, uint8_t event, uint8_t ep) {
    uint8_t buf[EP_OUT_SIZE];
    auto len = usbd_ep_read(dev, ep, buf, sizeof(buf));
    switch (event) {
    case usbd_evt_eptx:
        LL_GPIO_TogglePin(LED4_GPIO, LED4_PIN);
        break;
    case usbd_evt_eprx:
        break;
    case usbd_evt_reset:
    case usbd_evt_sof:
    case usbd_evt_susp:
    case usbd_evt_wkup:
    case usbd_evt_epsetup:

    case usbd_evt_error:
    case usbd_evt_count:
        LL_GPIO_SetOutputPin(LED5_GPIO, LED5_PIN);

        break;
    default:
        break;
    }
    (void)len;
}

void usb_midi::ep_tx(usbd_device *dev, uint8_t event, uint8_t ep) {
    switch (event) {
    case usbd_evt_reset:
    case usbd_evt_sof:
    case usbd_evt_susp:
    case usbd_evt_wkup:
    case usbd_evt_eptx:
    case usbd_evt_eprx:
    case usbd_evt_epsetup:
    case usbd_evt_error:
    case usbd_evt_count:
        LL_GPIO_SetOutputPin(LED5_GPIO, LED5_PIN);

        break;
    default:
        break;
    }
}

_usbd_respond usb_midi::setconf(usbd_device *dev, uint8_t cfg) {
    switch (cfg) {
    case 0:
        /* deconfiguring device */
        usbd_ep_deconfig(dev, EP_IN_ADDR);
        usbd_ep_deconfig(dev, EP_OUT_ADDR);
        usbd_reg_endpoint(dev, EP_IN_ADDR, nullptr);
        usbd_reg_endpoint(dev, EP_OUT_ADDR, nullptr);
        instance().curr_conf_ = configuration::not_active;
        return usbd_ack;
    case 1:
        /* configuring device */
        instance().curr_conf_ = configuration::active;
        usbd_ep_config(dev, EP_IN_ADDR, USB_EPTYPE_BULK, EP_IN_SIZE);
        usbd_ep_config(dev, EP_OUT_ADDR, USB_EPTYPE_BULK, EP_OUT_SIZE);

        usbd_reg_endpoint(dev, EP_IN_ADDR, ep_tx);
        usbd_reg_endpoint(dev, EP_OUT_ADDR, ep_rx);

        return usbd_ack;
    default:
        return usbd_fail;
    }
}

_usbd_respond usb_midi::getdesc(usbd_ctlreq *req, void **address,
                                uint16_t *length) {
    const uint8_t dtype = req->wValue >> 8;
    const uint8_t dnumber = req->wValue & 0xFF;
    const void *desc;
    uint16_t len = 0;
    switch (dtype) {
    case USB_DTYPE_DEVICE:
        desc = &usb_midi::dev_descriptor;
        break;
    case USB_DTYPE_CONFIGURATION:
        desc = &usb_midi::conf_descriptor;
        len = sizeof(usb_midi::conf_descriptor);
        break;
    case USB_DTYPE_STRING:
        if (dnumber < 3) {
            desc = str_table[dnumber];
        } else {
            return usbd_fail;
        }
        break;
    default:
        return usbd_fail;
    }
    if (len == 0) {
        len = ((struct usb_header_descriptor *)desc)->bLength;
    }
    *address = (void *)desc;
    *length = len;
    return usbd_ack;
}

_usbd_respond usb_midi::control(usbd_device *dev, usbd_ctlreq *req,
                                usbd_rqc_callback *callback) {
    if (((USB_REQ_RECIPIENT | USB_REQ_TYPE) & req->bmRequestType) ==
            (USB_REQ_INTERFACE | USB_REQ_CLASS) &&
        req->wIndex == 0) {
        switch (req->bRequest) {
        default:
            return usbd_fail;
        }
    }

    return usbd_fail;
}

void usb_midi::irq_handler() { usbd_poll(&instance().udev); }
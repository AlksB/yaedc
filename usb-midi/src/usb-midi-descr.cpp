#include "usb-midi.h"

constexpr usb_device_descriptor usb_midi::dev_descriptor = {
        .bLength = sizeof(usb_device_descriptor),
        .bDescriptorType = USB_DTYPE_DEVICE,
        .bcdUSB = VERSION_BCD(1, 10, 0),
        .bDeviceClass = USB_CLASS_PER_INTERFACE,
        .bDeviceSubClass = USB_SUBCLASS_NONE,
        .bDeviceProtocol = USB_PROTO_NONE,
        .bMaxPacketSize0 = EP0_SIZE,
        .idVendor = 0x1488,
        .idProduct = 0x1488,
        .bcdDevice = VERSION_BCD(0, 0, 0),
        .iManufacturer = 1,
        .iProduct = 2,
        .iSerialNumber = INTSERIALNO_DESCRIPTOR,
        .bNumConfigurations = 1,
};

constexpr usb_midi::config_descriptor_t usb_midi::conf_descriptor = {
        .configuration = {
                .bLength = sizeof(usb_config_descriptor),
                .bDescriptorType = USB_DTYPE_CONFIGURATION,
                .wTotalLength = sizeof(config_descriptor_t),
                .bNumInterfaces = 2,
                .bConfigurationValue = 0x01,
                .iConfiguration = 0x00,
                .bmAttributes = USB_CFG_ATTR_RESERVED,
                .bMaxPower = USB_CFG_POWER_MA(100),
        },

        .ac_if_st = {
                .bLength = sizeof(usb_interface_descriptor),
                .bDescriptorType = USB_DTYPE_INTERFACE,
                .bInterfaceNumber = 0x00,
                .bAlternateSetting = 0x00,
                .bNumEndpoints = 0x00,
                .bInterfaceClass = USB_CLASS_AUDIO,
                .bInterfaceSubClass = AUDIO_CSCP_ControlSubclass,
                .bInterfaceProtocol = 0x00,
                .iInterface = 0x00,
        },
        .ac_if_class = {
                .Length = sizeof(USB_Audio_Descriptor_Interface_AC_t),
                .Type = USB_DTYPE_CS_INTERFACE,
                .Subtype = AUDIO_DSUBTYPE_CSInterface_Header,
                .ACSpecification = VERSION_BCD(1, 0, 0),
                .TotalLength = sizeof(USB_Audio_Descriptor_Interface_AC_t),
                .InCollection = 0x01,
                .InterfaceNumber = 0x01,
        },

        .midi_streaming_if_st = {
                .bLength = sizeof(usb_interface_descriptor),
                .bDescriptorType = USB_DTYPE_INTERFACE,
                .bInterfaceNumber = 0x01,
                .bAlternateSetting = 0x00,
                .bNumEndpoints = 0x02,
                .bInterfaceClass = USB_CLASS_AUDIO,
                .bInterfaceSubClass = AUDIO_CSCP_MIDIStreamingSubclass,
                .bInterfaceProtocol = 0x00,
                .iInterface = 0x00,
        },
        .midi_streaming_if_class = {
                .Length = sizeof(USB_MIDI_Descriptor_AudioInterface_AS_t),
                .Type = USB_DTYPE_CS_INTERFACE,
                .Subtype = AUDIO_DSUBTYPE_CSInterface_Header,
                .AudioSpecification = VERSION_BCD(1, 0, 0),
                .TotalLength =   sizeof(USB_MIDI_Descriptor_AudioInterface_AS_t)
                                 + sizeof(USB_MIDI_Descriptor_InputJack_t)
                                 + sizeof(USB_MIDI_Descriptor_InputJack_t)
                                 + sizeof(USB_MIDI_Descriptor_OutputJack_t)
                                 + sizeof(USB_MIDI_Descriptor_OutputJack_t)
                                 + sizeof(usb_endpoint_descriptor)
                                 + sizeof(USB_MIDI_StdDescriptor_Jack_Endpoint_t)
                                 + sizeof(usb_endpoint_descriptor)
                                 + sizeof(USB_MIDI_StdDescriptor_Jack_Endpoint_t)
        },
        .in_jack_emb = {
                .Length = sizeof(USB_MIDI_Descriptor_InputJack_t),
                .Type = USB_DTYPE_CS_INTERFACE,
                .Subtype = AUDIO_DSUBTYPE_CSInterface_InputTerminal,
                .JackType = MIDI_JACKTYPE_Embedded,
                .JackID = 0x01,
                .JackStrIndex = 0x00,
        },
        .in_jack_ext = {
                .Length = sizeof(USB_MIDI_Descriptor_InputJack_t),
                .Type = USB_DTYPE_CS_INTERFACE,
                .Subtype = AUDIO_DSUBTYPE_CSInterface_InputTerminal,
                .JackType = MIDI_JACKTYPE_External,
                .JackID = 0x02,
                .JackStrIndex = 0x00,
        },
        .out_jack_emb = {
                .Length =sizeof(USB_MIDI_Descriptor_OutputJack_t),
                .Type = USB_DTYPE_CS_INTERFACE,
                .Subtype = AUDIO_DSUBTYPE_CSInterface_OutputTerminal,
                .JackType = MIDI_JACKTYPE_Embedded,
                .JackID =0x03,
                .NumberOfPins = 0x01,
                .SourceJackID = {0x02},
                .SourcePinID = {0x01},
                .JackStrIndex = 0x00,
        },
        .out_jack_ext = {
                .Length =sizeof(USB_MIDI_Descriptor_OutputJack_t),
                .Type = USB_DTYPE_CS_INTERFACE,
                .Subtype = AUDIO_DSUBTYPE_CSInterface_OutputTerminal,
                .JackType = MIDI_JACKTYPE_External,
                .JackID =0x04,
                .NumberOfPins = 0x01,
                .SourceJackID = {0x01},
                .SourcePinID = {0x01},
                .JackStrIndex = 0x00,
        },
        .out_ep_st = {
                .bLength = sizeof(usb_endpoint_descriptor),
                .bDescriptorType = USB_DTYPE_ENDPOINT,
                .bEndpointAddress = EP_OUT_ADDR,
                .bmAttributes = USB_EPTYPE_BULK,
                .wMaxPacketSize = EP_OUT_SIZE,
                .bInterval =0x00,
        },
        .out_ep_class = {
                .bLength = sizeof(USB_MIDI_StdDescriptor_Jack_Endpoint_t),
                .bDescriptorType = USB_DTYPE_CS_ENDPOINT,
                .bDescriptorSubtype = AUDIO_DSUBTYPE_CSEndpoint_General,
                .bNumEmbMIDIJack = 0x01,
                .bAssocJackID = {0x01},
        },
        .in_ep_st = {
                .bLength = sizeof(usb_endpoint_descriptor),
                .bDescriptorType = USB_DTYPE_ENDPOINT,
                .bEndpointAddress = EP_IN_ADDR,
                .bmAttributes = USB_EPTYPE_BULK,
                .wMaxPacketSize = EP_IN_SIZE,
                .bInterval = 0x00,
        },
        .in_ep_class = {
                .bLength = sizeof(USB_MIDI_StdDescriptor_Jack_Endpoint_t),
                .bDescriptorType = USB_DTYPE_CS_ENDPOINT,
                .bDescriptorSubtype = AUDIO_DSUBTYPE_CSEndpoint_General,
                .bNumEmbMIDIJack = 0x01,
                .bAssocJackID = {0x03},
        },
};

constexpr struct usb_string_descriptor  usb_midi::lang_desc = USB_ARRAY_DESC(USB_LANGID_ENG_US);

const usb_midi::prod_descr_t usb_midi::prod_descr = {
        .length = sizeof(usb_midi::prod_descr_t),
        .dtype = USB_DTYPE_STRING,
        .string = {'c', 'u', 'n', 't'},
};

const usb_midi::manuf_descr_t usb_midi::manuf_descr = {
        .length = sizeof(usb_midi::manuf_descr_t),
        .dtype = USB_DTYPE_STRING,
        .string = {'m', 'i', 'd', 'i', 't', 'e', 's', 't', 'e', 'n', 'i', 'n', 'g', 's'},
};


const void * usb_midi::str_table[3] = {
        static_cast<  const void *>(&lang_desc),
        static_cast<  const void *>(&manuf_descr),
        static_cast< const void *>(&prod_descr),
};
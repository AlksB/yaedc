#pragma once
#include "FreeRTOS.h"

#include "AudioClassCommon.h"
#include "MIDIClassCommon.h"
#include "errno.h"
#include "usb.h"
#include "usb_std.h"
#include "usbd_core.h"

class usb_midi {
  private:
    usb_midi(){};

  public:
    static int init();

    static usb_midi &instance() {
        static usb_midi obj{};
        return obj;
    }

    static void irq_handler();

  public:
    static int send(MIDI_EventPacket_t &event);
    static int send(MIDI_EventPacket_t *events, size_t num);

  private:
    static _usbd_respond setconf(usbd_device *dev, uint8_t cfg);

    static _usbd_respond control(usbd_device *dev, usbd_ctlreq *req,
                                 usbd_rqc_callback *callback);

    static _usbd_respond getdesc(usbd_ctlreq *req, void **address,
                                 uint16_t *length);

  private:
    static void ep_rx(usbd_device *dev, uint8_t event, uint8_t ep);

    static void ep_tx(usbd_device *dev, uint8_t event, uint8_t ep);

  private:
    usbd_device udev = {nullptr};
    uint32_t ubuf[0x20];
    enum class configuration : uint8_t { not_active, active };
    configuration curr_conf_ = configuration::not_active;

  private:
    static const uint8_t EP0_SIZE = 8;
    static const uint8_t EP_OUT_ADDR = 0x01;
    static const uint16_t EP_OUT_SIZE = 64;
    static const uint8_t EP_IN_ADDR = 0x81;
    static const uint16_t EP_IN_SIZE = 64;

  private:
    struct config_descriptor_t {
        usb_config_descriptor configuration;
        usb_interface_descriptor ac_if_st;
        USB_Audio_Descriptor_Interface_AC_t ac_if_class;

        usb_interface_descriptor midi_streaming_if_st;
        USB_MIDI_Descriptor_AudioInterface_AS_t midi_streaming_if_class;

        USB_MIDI_Descriptor_InputJack_t in_jack_emb;
        USB_MIDI_Descriptor_InputJack_t in_jack_ext;
        USB_MIDI_Descriptor_OutputJack_t out_jack_emb;
        USB_MIDI_Descriptor_OutputJack_t out_jack_ext;

        usb_endpoint_descriptor out_ep_st;
        USB_MIDI_StdDescriptor_Jack_Endpoint_t out_ep_class;
        usb_endpoint_descriptor in_ep_st;
        USB_MIDI_StdDescriptor_Jack_Endpoint_t in_ep_class;
    } __attribute__((packed));

    static const usb_device_descriptor dev_descriptor;
    static const config_descriptor_t conf_descriptor;

    static const struct usb_string_descriptor lang_desc;
    static const usb_string_descriptor manuf_desc_en;
    static const usb_string_descriptor prod_desc_en;

    struct manuf_descr_t {
        uint8_t length;
        uint8_t dtype;
        uint16_t string[14];
    } __attribute__((packed));

    struct prod_descr_t {
        uint8_t length;
        uint8_t dtype;
        uint16_t string[4];
    } __attribute__((packed));

    static const manuf_descr_t manuf_descr;
    static const prod_descr_t prod_descr;

    static const void *str_table[3];
};
